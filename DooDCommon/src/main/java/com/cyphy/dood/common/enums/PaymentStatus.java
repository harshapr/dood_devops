package com.cyphy.dood.common.enums;

public enum PaymentStatus {

	DONT_USE_THIS(0),PAYMENT_COMPLETED(1),PAYMENT_CANCELLED(2);
	
	
	int id = -1;
	
	private PaymentStatus(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	public static PaymentStatus getPaymentStatus(long id) {
        for (PaymentStatus es : PaymentStatus.values()) {
            if (es.id == id) {
                return es;
            }
        }
        throw new IllegalArgumentException();
	}
}
