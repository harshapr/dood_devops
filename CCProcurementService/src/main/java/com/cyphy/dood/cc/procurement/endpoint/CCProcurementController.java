package com.cyphy.dood.cc.procurement.endpoint;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cyphy.dood.cc.procurement.service.CCProcurementService;
import com.cyphy.dood.cc.procurement.vo.CCProcurementVO;
import com.cyphy.dood.common.endpoint.AbstractEndPoint;
import com.cyphy.dood.common.util.DoodException;
import com.cyphy.dood.common.vo.RequestVO;



@RefreshScope
@RestController
@RequestMapping
public class CCProcurementController extends AbstractEndPoint{
    
    @Autowired
    CCProcurementService agentProcurementService;
	
	
    
    enum GroupBy{
    	SUPPLIER,COLLECTION_CENTER,DATE,CC_INCHARGE
    }
	/**
	 * Add/Update CC Procurement.
	 * @param RequestVO
	 * @return
	 * @throws IrakshanException
	 */
	@RequestMapping(value = { "addorupdateprocurement" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> addOrUpdateProcurementDetails(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			CCProcurementVO ccProcurementVO = getObjectFromRequestParam(request, CCProcurementVO.class);
			ccProcurementVO = agentProcurementService.addOrUpdateProcurementDetails(ccProcurementVO);
			obj = setSuccessResponse(ccProcurementVO, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	} 
	
	
	/**
	 * List procurements by CC for current date
	 * @param RequestVO
	 * @return
	 * @throws IrakshanException
	 */
	@RequestMapping(value = { "listprocurementsbycc" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getProcurementsByCC(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			//Long ccId = getObjectFromRequestParam(request, Long.class);
			Map<String,Object> inputData = getObjectFromRequestParam(request, Map.class);
			List<CCProcurementVO> procurementList = agentProcurementService.listProcurementsByCC(Long.parseLong(inputData.get("ccId").toString()));
			if(inputData.get("groupBy") != null) {
				if(inputData.get("groupBy").equals(GroupBy.SUPPLIER.toString())) {
					obj = groupProcurementResult(procurementList, GroupBy.SUPPLIER);
				}
			}else {
				obj = procurementList;
			}
			obj = setSuccessResponse(obj, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	} 
	
	
	/**
	 *List CC procurements between  Procurement between dates
	 * @param RequestVO - Map<String,Object) - ccId,fromDate,toDate(Input dates in format "dd-MM-yyyy HH:mm:ss),groupBy
	 * @return
	 * @throws IrakshanException
	 */
	@RequestMapping(value = { "listprocurementsbyccbetweendates" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getProcurementsByCCBetweenDates(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Map<String,Object> inputData = getObjectFromRequestParam(request, Map.class);
			List<CCProcurementVO> procurementList = agentProcurementService.listProcurementsByCCBetweenDates(inputData);
			if(inputData.get("groupBy") != null) {
				if(inputData.get("groupBy").equals(GroupBy.SUPPLIER.toString())) {
					obj = groupProcurementResult(procurementList, GroupBy.SUPPLIER);
				}if(inputData.get("groupBy").equals(GroupBy.DATE.toString())) {
					obj = groupProcurementResult(procurementList, GroupBy.DATE);
				}
			}else {
				obj = procurementList;
			}
			obj = setSuccessResponse(obj, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	
	/**
	 * List procurements by all the procurements by Supplier
	 * @param RequestVO
	 * @return
	 * @throws IrakshanException
	 */
	@RequestMapping(value = { "listprocurementsbysupplier" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getProcurementsBySupplier(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			//Long supplierId = getObjectFromRequestParam(request, Long.class);
			Map<String,Object> inputData = getObjectFromRequestParam(request, Map.class);
			List<CCProcurementVO> procurementList = agentProcurementService.listProcurementsBySupplier(Long.parseLong(inputData.get("supplierId").toString()));
			if(inputData.get("groupBy") != null) {
				if(inputData.get("groupBy").equals(GroupBy.DATE.toString())) {
					obj = groupProcurementResult(procurementList, GroupBy.DATE);
				}
			}else {
				obj = procurementList;
			}
			obj = setSuccessResponse(procurementList, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
		
	
	/**
	 *List Supplier procurements between  Procurement between dates
	 * @param RequestVO - Map<String,Object) - supplierId,fromDate,toDate(Input dates in format "dd-MM-yyyy HH:mm:ss)
	 * @return
	 * @throws IrakshanException
	 */
	@RequestMapping(value = { "listprocurementsbysupplierbetweendates" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getProcurementsBySupplierBetweenDates(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Map<String,Object> inputData = getObjectFromRequestParam(request, Map.class);
			List<CCProcurementVO> procurementList = agentProcurementService.listProcurementsBySupplierBetweenDates(inputData);
			if(inputData.get("groupBy") != null) {
				if(inputData.get("groupBy").equals(GroupBy.DATE.toString())) {
					obj = groupProcurementResult(procurementList, GroupBy.DATE);
				}
			}else {
				obj = procurementList;
			}
			obj = setSuccessResponse(procurementList, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	/**
	 * Cancel Procurement
	 * @param RequestVO - Map<String,Object) - procurementId and remarks
	 * @return
	 * @throws DoodException
	 */
	@RequestMapping(value = { "cancelprocurement" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> cancelProcurement(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Map<String,Object> inputData = getObjectFromRequestParam(request, Map.class);
			List<CCProcurementVO> procurementList = agentProcurementService.cancelProcurement(inputData);
			obj = setSuccessResponse(procurementList, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	/**
	 * Group Procurement Result
	 * @param procurementList
	 * @param reportInput
	 * @return
	 */
	private LinkedHashMap<?, List<CCProcurementVO>> groupProcurementResult(List<CCProcurementVO> procurementList,
			GroupBy reportInput) {
		LinkedHashMap<?, List<CCProcurementVO>> groupedReportData = null;
		if (reportInput.equals(GroupBy.SUPPLIER)) {
			groupedReportData = (LinkedHashMap<String, List<CCProcurementVO>>) procurementList.stream()
					.collect(Collectors.groupingBy(CCProcurementVO::getSupplierName, LinkedHashMap::new,
							Collectors.mapping(item -> item, Collectors.toList())));
		} else if (reportInput.equals(GroupBy.CC_INCHARGE)) {
			groupedReportData = (LinkedHashMap<String, List<CCProcurementVO>>) procurementList.stream()
					.collect(Collectors.groupingBy(CCProcurementVO::getProcuredBy, LinkedHashMap::new,
							Collectors.mapping(item -> item, Collectors.toList())));
		} else if (reportInput.equals(GroupBy.COLLECTION_CENTER)) {
			groupedReportData = (LinkedHashMap<String, List<CCProcurementVO>>) procurementList.stream()
					.collect(Collectors.groupingBy(CCProcurementVO::getCcName, LinkedHashMap::new,
							Collectors.mapping(item -> item, Collectors.toList())));
		} else if (reportInput.equals(GroupBy.DATE)) {
			groupedReportData = (LinkedHashMap<String, List<CCProcurementVO>>) procurementList.stream()
					.collect(Collectors.groupingBy(CCProcurementVO::getProcuredOnDate, LinkedHashMap::new,
							Collectors.mapping(item -> item, Collectors.toList())));
		}

		return groupedReportData;
	}
}
