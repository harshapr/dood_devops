package com.cyphy.dood.common.enums;

public enum Status {

	DONT_USE_THIS(0),ACTIVE(1),INACTIVE(2),DELETED(3);
	
	
	int id = -1;
	
	private Status(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	
	public static Status getStatusForValue(int i) {
		for(Status e: Status.values()) {
		    if(e.id == i) {
		      return e;
		    }
		  }
		return null;
	}
}
