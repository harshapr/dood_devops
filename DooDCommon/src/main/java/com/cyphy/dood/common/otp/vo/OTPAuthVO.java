package com.cyphy.dood.common.otp.vo;

public class OTPAuthVO {
	
	private Long requestedBy;
	
	private String requestedFor;

	private String authUniqueKey;
	
	private String requestUniqueKey;

	public Long getRequestedBy() {
		return requestedBy;
	}

	public void setRequestedBy(Long requestedBy) {
		this.requestedBy = requestedBy;
	}

	public String getRequestedFor() {
		return requestedFor;
	}

	public void setRequestedFor(String requestedFor) {
		this.requestedFor = requestedFor;
	}

	public String getAuthUniqueKey() {
		return authUniqueKey;
	}

	public void setAuthUniqueKey(String authUniqueKey) {
		this.authUniqueKey = authUniqueKey;
	}

	public String getRequestUniqueKey() {
		return requestUniqueKey;
	}

	public void setRequestUniqueKey(String requestUniqueKey) {
		this.requestUniqueKey = requestUniqueKey;
	}
	
	
}
