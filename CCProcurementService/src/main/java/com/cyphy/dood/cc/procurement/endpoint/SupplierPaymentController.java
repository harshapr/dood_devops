package com.cyphy.dood.cc.procurement.endpoint;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cyphy.dood.cc.procurement.service.SupplierPaymentService;
import com.cyphy.dood.cc.procurement.vo.SupplierPaymentVO;
import com.cyphy.dood.cc.procurement.vo.SupplierVO;
import com.cyphy.dood.common.endpoint.AbstractEndPoint;
import com.cyphy.dood.common.util.DoodException;
import com.cyphy.dood.common.vo.RequestVO;


@RefreshScope
@RestController
@RequestMapping
public class SupplierPaymentController extends AbstractEndPoint{
    
    @Autowired
    SupplierPaymentService supplierPaymentService;
	
	
    /**
	 * Make payment supplier.
	 * @param agentToSupplierPayment
	 * @return
	 * @throws DoodException
	 */
	@RequestMapping(value = { "makeorupdatepayment" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> makeOrUpdatePaymentToSupplier(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			SupplierPaymentVO agentToSupplierPaymentVO = getObjectFromRequestParam(request, SupplierPaymentVO.class);
			agentToSupplierPaymentVO = supplierPaymentService.makeOrUpdatePaymentToSupplier(agentToSupplierPaymentVO);
			obj = setSuccessResponse(agentToSupplierPaymentVO, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	} 
	
	
	/**
	 * Gets the Collection Center Dashboard Data.
	 * Dashboard shows Supplier pending payment details in the descending order(Amount to be paid).
	 * Input is Collection Center Id.
	 * 
	 */
	@RequestMapping(value = { "getccdashboarddata" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> findPendingPayments(@RequestBody RequestVO request) throws DoodException{
		Object obj = null;
		try {
			Long ccId = getObjectFromRequestParam(request, Long.class);
			List<SupplierVO> responseObject = supplierPaymentService.findPendingPaymentsOfCC(ccId);
			obj = setSuccessResponse(responseObject, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	/**
	 * Gets latest 10 payment history records of the supplier id given
	 * @return
	 * @throws DoodException
	 */
	@RequestMapping(value = { "getpendingandlastpaidamountbysupplier" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getPendingAndLastPaidAmountBysupplier(@RequestBody RequestVO request) throws DoodException{
		Object obj = null;
		try {
			Long supplierId = getObjectFromRequestParam(request, Long.class);
			Map<String,Object> returnJson = supplierPaymentService.getPendingAndLastPaidAmtTBySupplier(supplierId);
			obj = setSuccessResponse(returnJson, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	/**
	 * Gets latest 10 payment history records of the supplier id given
	 * @return
	 * @throws DoodException
	 */
	@RequestMapping(value = { "getpaymentmadetosupplier" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getPaymentHistoryBySupplierId(@RequestBody RequestVO request) throws DoodException{
		Object obj = null;
		try {
			Long supplierId = getObjectFromRequestParam(request, Long.class);
			List<SupplierPaymentVO> supplierPaymentList = supplierPaymentService.getPaymentMadeToSupplier(supplierId);
			obj = setSuccessResponse(supplierPaymentList, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	
	/**
	 * Gets payment history records of the supplier id given between the given dates
	 * @return
	 * @throws DoodException
	 */
	@RequestMapping(value = { "getpaymentmadetosupplierbetweendates" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getPaymentHistoryBySupplierIdBetweenDates(@RequestBody RequestVO request) throws DoodException{
		Object obj = null;
		try {
			Map<String, Object> requestData = getObjectFromRequestParam(request, Map.class);
			List<SupplierPaymentVO> supplierPaymentList = supplierPaymentService.getPaymentMadeToSupplierBetweenDates(((Integer)requestData.get("supplierId")).longValue(),(requestData.get("fromDate").toString()),(requestData.get("toDate").toString()));
			obj = setSuccessResponse(supplierPaymentList, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	/**
	 * Get the payments made by collection center. Gets only last 10 transactions. 
	 * @param Long - Collection Center Id
	 * @return
	 * @throws DoodException
	 */
	@RequestMapping(value = { "getpaymentsmadebycc" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getPaymentsMadeByCollectionCenter(@RequestBody RequestVO request){
		Object object = null;
		try {
			Long ccId = getObjectFromRequestParam(request, Long.class);
			List<SupplierPaymentVO> collectionCenterPaymentHistory =  supplierPaymentService.getPaymentsMadeByCollectionCenter(ccId);
			object = setSuccessResponse(collectionCenterPaymentHistory,request);
		}catch (Exception e) {
			e.printStackTrace();
			object = setErrorResponse(e,request);
		}
		return new ResponseEntity<T>((T) object, HttpStatus.OK);
	}
	
	/**
	 * Get the payments made by collection center between dates 
	 * @param Long - Collection Center Id
	 * @param Date - From date
	 * @param Date - To date
	 * @return
	 * @throws DoodException
	 */
	@RequestMapping(value = { "getpaymentsmadebyccbetweendates" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getPaymentsMadeByCollectionCenterBetweenDates(@RequestBody RequestVO request){
		Object object = null;
		try {
			Map<String, Object> requestData = getObjectFromRequestParam(request, Map.class);
			List<SupplierPaymentVO> supplierPaymentHistory =  supplierPaymentService.getPaymentsMadeByCollectionCenterBetweenDates(((Integer)requestData.get("ccId")).longValue(),(requestData.get("fromDate").toString()),(requestData.get("toDate").toString()));
			object = setSuccessResponse(supplierPaymentHistory,request);
		}catch (Exception e) {
			e.printStackTrace();
			object = setErrorResponse(e,request);
		}
		return new ResponseEntity<T>((T) object, HttpStatus.OK);
	}
	
	/**
	 * Find the payment by payment Id.
	 * @param agentId
	 * @param paymentId
	 * @return
	 * @throws DoodException
	 */
	@RequestMapping(value = { "getpaymentsdetailsbyid" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> findPaymentByPaymentId(@RequestBody RequestVO request) throws DoodException{
		Object obj = null;
		try {
			Map<String,Object> inputData = getObjectFromRequestParam(request, Map.class);
			SupplierPaymentVO paymentDetails = supplierPaymentService.findPaymentByPaymentId(Long.parseLong(inputData.get("agentId").toString()), Long.parseLong(inputData.get("paymentId").toString()));
			obj = setSuccessResponse(paymentDetails, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	
	/**
	 * Find the payment by payment ref number.
	 * @param agentId
	 * @param paymentId
	 * @return
	 * @throws DoodException
	 */
	@RequestMapping(value = { "getpaymentsdetailsbyrefnumber" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> findPaymentByPaymentRefNumber(@RequestBody RequestVO request) throws DoodException{
		Object obj = null;
		try {
			Map<String,Object> inputData = getObjectFromRequestParam(request, Map.class);
			SupplierPaymentVO paymentDetails = supplierPaymentService.findPaymentByPaymentRefNumber(Long.parseLong(inputData.get("agentId").toString()), inputData.get("paymentRefNumber").toString());
			obj = setSuccessResponse(paymentDetails, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	
	
	
	/**
	 * Cancel Payment.Once cancelled cannot be reverted. 
	 * @param paymentId
	 * @return
	 * @throws DoodException
	 */
	@RequestMapping(value = { "cancelpayment" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> cancelProcurement(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Long paymentId = getObjectFromRequestParam(request, Long.class);
			boolean returnValue = supplierPaymentService.cancelPayment(paymentId);
			obj = setSuccessResponse(returnValue, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	
	private Date getDate(String date) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return sdf.parse(date);
	}
	
}
