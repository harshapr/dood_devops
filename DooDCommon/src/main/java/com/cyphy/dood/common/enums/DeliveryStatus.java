package com.cyphy.dood.common.enums;

public enum DeliveryStatus {

	DONT_USE_THIS(0),IN_TRANSIT(1),RECEIVED_AND_ACCEPTED(2),NOT_RECEIVED(3),REJECTED(4),CANCELLED(5);
	
	
	int id = -1;
	
	private DeliveryStatus(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	
	public static DeliveryStatus getDeliveryStatus(int i) {
		for(DeliveryStatus e: DeliveryStatus.values()) {
		    if(e.id == i) {
		      return e;
		    }
		  }
		return null;
	}
}
