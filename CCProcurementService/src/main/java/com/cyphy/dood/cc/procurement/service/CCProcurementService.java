package com.cyphy.dood.cc.procurement.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.cyphy.dood.cc.procurement.vo.CCProcurementVO;
import com.cyphy.dood.cc.procurement.vo.SupplierVO;
import com.cyphy.dood.common.enums.ProcurementStatus;
import com.cyphy.dood.common.service.AbstractService;
import com.cyphy.dood.common.util.DoodException;
import com.cyphy.dood.common.vo.RequestVO;
import com.cyphy.dood.common.vo.ResponseVO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class CCProcurementService extends AbstractService{

	@Autowired
	Logger logger;
	
	
	@Autowired
	ObjectMapper objectMapper;
	
	/**
	 * Add or update CC procurement details
	 * @param ccProcurementVO
	 * @return
	 * URL = /cc/procurement/dao/addorupdate
	 */
	public CCProcurementVO addOrUpdateProcurementDetails(CCProcurementVO ccProcurementVO) throws DoodException{
		 
		if(ccProcurementVO.getId() != null) {
			RequestVO requestVO = new RequestVO();
			requestVO.setRequestParams(ccProcurementVO.getId());
			ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/procurement/getprocurementbyid", requestVO);
			CCProcurementVO ccProcurementVOFromDB  = objectMapper.convertValue(result.getBody().getReturnObject(), CCProcurementVO.class);
			if(ccProcurementVOFromDB != null) {
				ccProcurementVO.setMo(new Timestamp(System.currentTimeMillis()));
				ccProcurementVO.setProcurementDateTime(ccProcurementVOFromDB.getProcurementDateTime());
				ccProcurementVO.setRemarks(ccProcurementVOFromDB.getRemarks());
				ccProcurementVO.setProcurementStatus(ccProcurementVOFromDB.getProcurementStatus());
				ccProcurementVO.setRefNo(ccProcurementVOFromDB.getRefNo());
				ccProcurementVO.setAmount(ccProcurementVOFromDB.getAmount());
			}
		}else {
			ccProcurementVO.setProcurementDateTime(new Timestamp(System.currentTimeMillis()));
			ccProcurementVO.setProcurementStatus(ProcurementStatus.PROCURED_FROM_SUPPLIER);
			ccProcurementVO.setRefNo(generateRefNumber());
		}
		
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(ccProcurementVO);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/procurement/addorupdate", requestVO);
		ccProcurementVO = objectMapper.convertValue(result.getBody().getReturnObject(), CCProcurementVO.class);
		//fetch latest records
		requestVO.setRequestParams(ccProcurementVO.getId());
		result = doPostCall("/repository/cc/procurement/getprocurementbyid", requestVO);
		ccProcurementVO  = objectMapper.convertValue(result.getBody().getReturnObject(), CCProcurementVO.class);

		return ccProcurementVO;
	}
	
	
	/**
	 * Lists all the procurements by Collection Center  
	 * @param ccId - Collection Center Id
	 * @return
	 * @throws DoodException
	 */
	public List<CCProcurementVO> listProcurementsByCC(Long ccId) throws DoodException{
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(ccId);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/procurement/listprocurementsbycc", requestVO);
		List<CCProcurementVO> listOfProcurements = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<CCProcurementVO>>() {});
		return listOfProcurements;
	}
	
	
	/**
	 * Lists all the procurements by Supplier  
	 * @param supplierId
	 * @return
	 * @throws DoodException
	 */
	public List<CCProcurementVO> listProcurementsBySupplier(Long supplierId) throws DoodException{
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(supplierId);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/procurement/listprocurementsbysupplier", requestVO);
		List<CCProcurementVO> listOfProcurements = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<CCProcurementVO>>() {});
		return listOfProcurements;
	}
	
	
	/**
	 * Lists all the procurements by Supplier between dates
	 * @param inputData - ccId,fromDate,toDate(Input dates in format "dd-MM-yyyy HH:mm:ss)
	 * @return
	 * @throws DoodException
	 */
	public List<CCProcurementVO> listProcurementsBySupplierBetweenDates(Map<String,Object> inputData) throws DoodException{
		try {
			RequestVO requestVO = new RequestVO();
			requestVO.setRequestParams(inputData);
			ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/procurement/listprocurementsbysupplierbetweendates", requestVO);
			List<CCProcurementVO> listOfProcurements = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<CCProcurementVO>>() {});
			return listOfProcurements;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Lists all the procurements by Collection Center between dates
	 * @param inputData - ccId,fromDate,toDate(Input dates in format "dd-MM-yyyy HH:mm:ss)
	 * @return
	 * @throws DoodException
	 */
	public List<CCProcurementVO> listProcurementsByCCBetweenDates(Map<String,Object> inputData) throws DoodException{
		try {
			RequestVO requestVO = new RequestVO();
			requestVO.setRequestParams(inputData);
			ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/procurement/listprocurementsbyccbetweendates", requestVO);
			List<CCProcurementVO> listOfProcurements = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<CCProcurementVO>>() {});
			return listOfProcurements;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Cancel Procurement 
	 * @param inputData - procurementId,remarks
	 * @return
	 * @throws DoodException
	 */
	public List<CCProcurementVO> cancelProcurement(Map<String,Object> inputData) throws DoodException{
		try {
			RequestVO requestVO = new RequestVO();
			requestVO.setRequestParams(inputData);
			ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/procurement/cancelprocurement", requestVO);
			List<CCProcurementVO> listOfProcurements = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<CCProcurementVO>>() {});
			return listOfProcurements;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	 
	
	private String generateRefNumber() {
	    Random rnd = new Random();
	    int number = rnd.nextInt(9999);

	    // this will convert any number sequence into 6 character.
	    String randomNumber =  String.format("%04d", number);
	    SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
	    String dateFormat = sdf.format(new Date());
	    
	    return "PR_REF_"+dateFormat+"_"+randomNumber;
	}
}
