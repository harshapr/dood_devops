package com.cyphy.dood.cc.procurement.vo;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.cyphy.dood.common.enums.DeliveryStatus;
import com.cyphy.dood.common.enums.MilkType;
import com.fasterxml.jackson.annotation.JsonFormat;

public class CCBMCDeliveryVO implements Serializable {

	private Long id;

	
	private String challanRefNo;
	
	
	private Long ccId;
	
	
	private Long bmcId;

	
	private int  noOfCans;

	
	private int noOflts;
	
	
	private String vehicleNumber;

	
	private String driverName;

	
	
	private DeliveryStatus deliveryStatus;
	
	private MilkType milkType;
	
	
	public MilkType getMilkType() {
		return milkType;
	}

	public void setMilkType(MilkType milkType) {
		this.milkType = milkType;
	}

	private Long receivedBy;
	
	private LocalDateTime receivedOn;
	
	
	private String remarks;
	
	
	private Long generatedBy;
	
	
	private LocalDateTime generatedOn = LocalDateTime.now();
	
	

	private String ccName;
	

	private String bmcName;
	
	private String challanGeneratedDate;
	

	private String challanGeneratedTime;
	

	private String generatedByName;
	

	private String receivedByBMCDate;
	

	private String receivedByBMCTime;
	

	private String receivedByName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getChallanRefNo() {
		return challanRefNo;
	}

	public void setChallanRefNo(String challanRefNo) {
		this.challanRefNo = challanRefNo;
	}

	public Long getCcId() {
		return ccId;
	}

	public void setCcId(Long ccId) {
		this.ccId = ccId;
	}

	public Long getBmcId() {
		return bmcId;
	}

	public void setBmcId(Long bmcId) {
		this.bmcId = bmcId;
	}

	public int getNoOfCans() {
		return noOfCans;
	}

	public void setNoOfCans(int noOfCans) {
		this.noOfCans = noOfCans;
	}

	public int getNoOflts() {
		return noOflts;
	}

	public void setNoOflts(int noOflts) {
		this.noOflts = noOflts;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}


	public DeliveryStatus getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public Long getReceivedBy() {
		return receivedBy;
	}

	public void setReceivedBy(Long receivedBy) {
		this.receivedBy = receivedBy;
	}

	public LocalDateTime getReceivedOn() {
		return receivedOn;
	}

	public void setReceivedOn(LocalDateTime receivedOn) {
		this.receivedOn = receivedOn;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getGeneratedBy() {
		return generatedBy;
	}

	public void setGeneratedBy(Long generatedBy) {
		this.generatedBy = generatedBy;
	}

	public LocalDateTime getGeneratedOn() {
		return generatedOn;
	}

	public void setGeneratedOn(LocalDateTime generatedOn) {
		this.generatedOn = generatedOn;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public String getBmcName() {
		return bmcName;
	}

	public void setBmcName(String bmcName) {
		this.bmcName = bmcName;
	}



	public String getChallanGeneratedDate() {
		return challanGeneratedDate;
	}

	public void setChallanGeneratedDate(String challanGeneratedDate) {
		this.challanGeneratedDate = challanGeneratedDate;
	}

	public String getChallanGeneratedTime() {
		return challanGeneratedTime;
	}

	public void setChallanGeneratedTime(String challanGeneratedTime) {
		this.challanGeneratedTime = challanGeneratedTime;
	}

	public String getGeneratedByName() {
		return generatedByName;
	}

	public void setGeneratedByName(String generatedByName) {
		this.generatedByName = generatedByName;
	}

	public String getReceivedByBMCDate() {
		return receivedByBMCDate;
	}

	public void setReceivedByBMCDate(String receivedByBMCDate) {
		this.receivedByBMCDate = receivedByBMCDate;
	}

	public String getReceivedByBMCTime() {
		return receivedByBMCTime;
	}

	public void setReceivedByBMCTime(String receivedByBMCTime) {
		this.receivedByBMCTime = receivedByBMCTime;
	}

	public String getReceivedByName() {
		return receivedByName;
	}

	public void setReceivedByName(String receivedByName) {
		this.receivedByName = receivedByName;
	}
			

}
