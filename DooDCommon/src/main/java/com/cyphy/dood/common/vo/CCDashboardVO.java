package com.cyphy.dood.common.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class CCDashboardVO {

	private Integer totalProcurements = 0;

	private Integer cancelledProcurements = 0;

	@JsonSerialize(using = DecimalJsonSerializer.class)
	private Double totalQuantity = 0.0;

	@JsonSerialize(using = DecimalJsonSerializer.class)
	private Double cancelledQuantity = 0.0;

	@JsonSerialize(using = DecimalJsonSerializer.class)
	private Double cowMilkQuantity = 0.0;

	@JsonSerialize(using = DecimalJsonSerializer.class)
	private Double buffaloMilkQuantity = 0.0;

	@JsonSerialize(using = DecimalJsonSerializer.class)
	private Double totalAmount = 0.0;

	@JsonSerialize(using = DecimalJsonSerializer.class)
	private Double totalAmountPaid = 0.0;

	public CCDashboardVO() {
	}

	public CCDashboardVO(Integer totalProcurements, Integer cancelledProcurements, Double totalQuantity,
			Double cancelledQuantity, Double cowMilkQuantity, Double buffaloMilkQuantity, Double totalAmount,
			Double totalAmountPaid) {
		super();
		this.totalProcurements = totalProcurements;
		this.cancelledProcurements = cancelledProcurements;
		this.totalQuantity = totalQuantity;
		this.cancelledQuantity = cancelledQuantity;
		this.cowMilkQuantity = cowMilkQuantity;
		this.buffaloMilkQuantity = buffaloMilkQuantity;
		this.totalAmount = totalAmount;
		this.totalAmountPaid = totalAmountPaid;
	}
	public String getTotalProcurements() {
		return totalProcurements.toString();
	}

	public void setTotalProcurements(Integer totalProcurements) {
		this.totalProcurements = totalProcurements;
	}

	public String getCancelledProcurements() {
		return cancelledProcurements.toString();
	}

	public void setCancelledProcurements(Integer cancelledProcurements) {
		this.cancelledProcurements = cancelledProcurements;
	}

	public Double getCowMilkQuantity() {
		return cowMilkQuantity;
	}

	public void setCowMilkQuantity(Double cowMilkQuantity) {
		this.cowMilkQuantity = cowMilkQuantity;
	}

	public Double getBuffaloMilkQuantity() {
		return buffaloMilkQuantity;
	}

	public void setBuffaloMilkQuantity(Double buffaloMilkQuantity) {
		this.buffaloMilkQuantity = buffaloMilkQuantity;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getTotalAmountPaid() {
		return totalAmountPaid;
	}

	public void setTotalAmountPaid(Double totalAmountPaid) {
		this.totalAmountPaid = totalAmountPaid;
	}

	public Double getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Double getCancelledQuantity() {
		return cancelledQuantity;
	}

	public void setCancelledQuantity(Double cancelledQuantity) {
		this.cancelledQuantity = cancelledQuantity;
	}

}
