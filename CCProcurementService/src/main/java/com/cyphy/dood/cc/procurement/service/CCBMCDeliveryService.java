package com.cyphy.dood.cc.procurement.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.cyphy.dood.cc.procurement.vo.CCBMCDeliveryVO;
import com.cyphy.dood.cc.procurement.vo.CCProcurementVO;
import com.cyphy.dood.cc.procurement.vo.SupplierPaymentVO;
import com.cyphy.dood.common.enums.DeliveryStatus;
import com.cyphy.dood.common.service.AbstractService;
import com.cyphy.dood.common.util.DoodException;
import com.cyphy.dood.common.vo.RequestVO;
import com.cyphy.dood.common.vo.ResponseVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class CCBMCDeliveryService extends AbstractService{

	@Autowired
	Logger logger;
	
	
	@Autowired
	ObjectMapper objectMapper;
	
	/**
	 * Generate delivery Challan
	 * @param CCBMCDeliveryVO
	 * @return
	 * URL = /agent/procurement/dao/addorupdate
	 */
	public String generateCCToBMCDeliveryChallan(CCBMCDeliveryVO ccBMCDeliveryVO) throws DoodException{
		RequestVO requestVO = new RequestVO();
		ccBMCDeliveryVO.setChallanRefNo("CC_BMC_DC_"+System.currentTimeMillis());
		ccBMCDeliveryVO.setDeliveryStatus(DeliveryStatus.IN_TRANSIT);
		requestVO.setRequestParams(ccBMCDeliveryVO);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/bmcdelivery/generatedeliverychallan", requestVO);
		ccBMCDeliveryVO = objectMapper.convertValue(result.getBody().getReturnObject(), CCBMCDeliveryVO.class);
		return ccBMCDeliveryVO.getChallanRefNo();
	}
	
	
	/**
	 * Gets the delivery details by challan Ref number
	 * @param challanRefNo
	 * @return
	 * @throws DoodException
	 */
	public CCBMCDeliveryVO getDeliveryDetailsByChallanRefNo(String challanRefNo) throws DoodException{
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(challanRefNo);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/bmcdelivery/getdeliverydetailsbychallanrefno", requestVO);
		CCBMCDeliveryVO ccBMCDeliveryVO = objectMapper.convertValue(result.getBody().getReturnObject(), CCBMCDeliveryVO.class);
		return ccBMCDeliveryVO;
	}
	
	/**
	 * List Collection Center Deliveries for the current date
	 * @param ccId
	 * @return
	 * @throws Exception
	 */
	public List<CCBMCDeliveryVO> listDeliveriesByCC(Long ccId) throws Exception{
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(ccId);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/bmcdelivery/listdeliveriesbycc", requestVO);
		List<CCBMCDeliveryVO> ccDeliveries= objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<CCBMCDeliveryVO>>() {});
		return ccDeliveries;
	}
	
	
	/**
	 * List Collection Center Deliveries for the current date
	 * @param ccId
	 * @return
	 * @throws Exception
	 */
	public List<CCBMCDeliveryVO> listDeliveriesByCCBetweenDates(Long ccId,String fromDate,String toDate) throws Exception{
		RequestVO requestVO = new RequestVO();
    	Map<String,Object> requestData = new HashMap<String,Object>();
    	requestData.put("ccId",ccId);
    	requestData.put("fromDate",fromDate);
    	requestData.put("toDate",toDate);
		requestVO.setRequestParams(requestData);
		
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/bmcdelivery/listdeliveriesbyccbetweendates", requestVO);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		List<CCBMCDeliveryVO> ccDeliveries= objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<CCBMCDeliveryVO>>() {});
		return ccDeliveries;
	}
	
	
	/**
	 * Updates Delivery status
	 * @param deliveryId
	 * @param deliveryStatus
	 * @param remarks
	 * @return
	 * @throws Exception
	 */
	public boolean updateDeliveryStatus(Long deliveryId,DeliveryStatus deliveryStatus,String remarks,int totalLtsAccepted) throws Exception{
		RequestVO requestVO = new RequestVO();
    	Map<String,Object> requestData = new HashMap<String,Object>();
    	requestData.put("deliveryId",deliveryId);
    	requestData.put("deliveryStatus",deliveryStatus);
    	requestData.put("remarks",remarks);
    	requestData.put("totalLtsAccepted",totalLtsAccepted);
		requestVO.setRequestParams(requestData);
		
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/bmcdelivery/updatedeliverystatus", requestVO);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		boolean returnValue = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<Boolean>() {});
		return returnValue;
	}

	/**
	 * List Deliveries by BMC for the current date
	 * @param ccId
	 * @return
	 * @throws Exception
	 */
	public Object listDeliveriesByBMC(Long bmcId) {
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(bmcId);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/bmcdelivery/listdeliveriesbybmc", requestVO);
		List<CCBMCDeliveryVO> ccDeliveries= objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<CCBMCDeliveryVO>>() {});
		return ccDeliveries;
	}


	/**
	 * List Deliveries by BMC between dates
	 * @param requestData
	 * @return
	 */
	public Object listDeliveriesByCCBetweenDates(Map<String, Object> requestData) {
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(requestData);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/bmcdelivery/listdeliveriesbybmcbetweendates", requestVO);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		List<CCBMCDeliveryVO> ccDeliveries= objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<CCBMCDeliveryVO>>() {});
		return ccDeliveries;
	}

}
