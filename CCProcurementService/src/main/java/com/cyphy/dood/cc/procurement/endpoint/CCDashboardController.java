package com.cyphy.dood.cc.procurement.endpoint;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cyphy.dood.cc.procurement.service.CCDashBoardService;
import com.cyphy.dood.common.endpoint.AbstractEndPoint;
import com.cyphy.dood.common.util.DoodException;
import com.cyphy.dood.common.vo.CCDashboardVO;
import com.cyphy.dood.common.vo.RequestVO;


@RefreshScope
@RestController
@RequestMapping
public class CCDashboardController extends AbstractEndPoint{

	@Autowired
	CCDashBoardService ccDashboardService;
	
	
	/**
	 * fetch dashboard data for CC Procurement details
	 * @param RequestVO
	 * @return
	 * @throws IrakshanException
	 */
	@RequestMapping(value = { "dashboard/ccprocurementdetails" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> fetchCCProcurementDashboardDetails(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Map<String,Object> inputData = getObjectFromRequestParam(request, Map.class);
			CCDashboardVO ccDahsboardVO = ccDashboardService.fetchCCProcurementDashboardDetails(inputData);
			obj = setSuccessResponse(ccDahsboardVO, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	} 
}
