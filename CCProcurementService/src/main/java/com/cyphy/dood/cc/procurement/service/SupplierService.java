package com.cyphy.dood.cc.procurement.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.cyphy.dood.cc.procurement.vo.SupplierVO;
import com.cyphy.dood.common.enums.Status;
import com.cyphy.dood.common.service.AbstractService;
import com.cyphy.dood.common.util.DoodException;
import com.cyphy.dood.common.vo.RequestVO;
import com.cyphy.dood.common.vo.ResponseVO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SupplierService extends AbstractService{

	@Autowired
	Logger logger;
	
	
	@Autowired
	ObjectMapper objectMapper;
	
	/**
	 * Add or update Supplier
	 * @param agentProcurementVO
	 * @return
	 * URL = /agent/procurement/dao/addorupdate
	 */
	public SupplierVO addOrUpdateSupplier(SupplierVO supplierVO) throws DoodException{
		if(supplierVO.getId() != null) {
//			RequestVO requestVO = new RequestVO();
//			requestVO.setRequestParams(supplierVO.getId());
		//	ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/supplier/getsupplierbyid", requestVO);
			List<SupplierVO> supplierVoFromDBList = getSupplierByMobileNumber(supplierVO.getPhoneNumber());
			SupplierVO supplierVOFromDB = null;
			if(supplierVoFromDBList != null) {
				if(supplierVoFromDBList.get(0).getId().equals(supplierVO.getId())) {
					supplierVOFromDB = supplierVoFromDBList.get(0);
				//	supplierVO.setCb(supplierVOFromDB.getCb());
				//	supplierVO.setCo(supplierVOFromDB.getCo());
					supplierVO.setRegNumber(supplierVOFromDB.getRegNumber());
					supplierVO.setStatus(supplierVOFromDB.getStatus());
				}else {
					throw new DoodException("WD_BVAL_FAIL_FAR_DUP_MOB");
				}
			supplierVO.setMo(new Timestamp(System.currentTimeMillis()));
			}
		}else {
			List<SupplierVO> supplierWithMobNumber = getSupplierByMobileNumber(supplierVO.getPhoneNumber());
			if(supplierWithMobNumber == null || supplierWithMobNumber.isEmpty()) {
				supplierVO.setCo(new Timestamp(System.currentTimeMillis()));
				supplierVO.setStatus(Status.ACTIVE);
				supplierVO.setRegNumber("WD_FAR_"+getRandomNumberString());
			}else {
				throw new DoodException("WD_BVAL_FAIL_FAR_DUP_MOB");
			}
		}
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(supplierVO);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/supplier/addorupdate", requestVO);
		supplierVO = objectMapper.convertValue(result.getBody().getReturnObject(), SupplierVO.class);
		return supplierVO;
	}
	
	
	/**
	 * Lists all the suppliers created by the cc 
	 * @param ccId
	 * @return
	 * @throws DoodException
	 */
	public List<SupplierVO> listCCSuppliers(Long ccId) throws DoodException{
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(ccId);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/supplier/listsuppliersbycc", requestVO);
		List<SupplierVO> listOfSuppliers = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<SupplierVO>>() {});
		return listOfSuppliers;
	}
	
	
	/**
	 * Get the supplier details by reg number. 
	 * @param supplierRegNo
	 * @return
	 * @throws DoodException
	 */
	public SupplierVO getSupplierByRegNo(String supplierRegNo) throws DoodException{
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(supplierRegNo);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/supplier/getsupplierbyregno", requestVO);
		SupplierVO supplierDetails = objectMapper.convertValue(result.getBody().getReturnObject(),  new TypeReference<SupplierVO>() {});
		return supplierDetails;
	}
	
	
	/**
	 * Get the supplier details by Id Pk 
	 * @param supplierId
	 * @return
	 * @throws DoodException
	 */
	public SupplierVO getSupplierById(Long supplierId) throws DoodException{
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(supplierId);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/supplier/getsupplierbyid", requestVO);
		if(result.getBody().getReturnObject() != null) {
			SupplierVO listOfSuppliers = objectMapper.convertValue(result.getBody().getReturnObject(),  new TypeReference<SupplierVO>() {});
			return listOfSuppliers;
		}
		return null;
	}
	
	
	/**
	 * Get the supplier details by reg number. 
	 * @param supplierRegNo
	 * @return
	 * @throws DoodException
	 */
	public List<SupplierVO> getSupplierByMobileNumber(String mobileNumber) throws DoodException{
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(mobileNumber);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/supplier/getsupplierbymobileno", requestVO);
		if(result.getBody().getReturnObject() != null) {
			List<SupplierVO> listOfSuppliers = objectMapper.convertValue(result.getBody().getReturnObject(),  new TypeReference<List<SupplierVO>>() {});
			return listOfSuppliers;
		}
		return null;
	}
	
	
	/**
	 * Get the supplier details by Name or Mobile Number
	 * @param nameormobilenumber
	 * @return
	 * @throws DoodException
	 */
	public List<SupplierVO> getSupplierByNameOrMobileNumber(String mobileNumber) throws DoodException{
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(mobileNumber);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/supplier/getsupplierbynameormobileno", requestVO);
		if(result.getBody().getReturnObject() != null) {
			List<SupplierVO> listOfSuppliers = objectMapper.convertValue(result.getBody().getReturnObject(),  new TypeReference<List<SupplierVO>>() {});
			return listOfSuppliers;
		}
		return null;
	}

	/**
	 * Delete the supplier
	 * @param inputData - SupplierId and Remarks
	 * @return
	 * @throws DoodException
	 */
	public boolean deleteSupplier(Long supplierId) throws DoodException{
		try {
			RequestVO requestVO = new RequestVO();
			requestVO.setRequestParams(supplierId);
			ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/supplier/deletesupplier", requestVO);
			if(result.getBody().isSuccess()) {
				return true;
			}else {
				return false;
			}
		}catch(Exception e) {
			throw new DoodException("Exception occurred while deleting the supplier", e);
		}
	}
	
	
	public static String getRandomNumberString() {
	    // It will generate 7 digit random Number.
	    // from 0 to 999999
	    Random rnd = new Random();
	    int number = rnd.nextInt(9999999);

	    // this will convert any number sequence into 6 character.
	    return String.format("%07d", number);
	}
	
}
