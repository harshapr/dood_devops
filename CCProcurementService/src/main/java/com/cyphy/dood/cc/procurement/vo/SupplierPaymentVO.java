package com.cyphy.dood.cc.procurement.vo;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.cyphy.dood.common.enums.PaymentMode;
import com.cyphy.dood.common.enums.PaymentStatus;
import com.cyphy.dood.common.vo.DecimalJsonSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
 
@JsonInclude(Include.NON_NULL)
public class SupplierPaymentVO implements Serializable {

	private Long id;

	private Long supplierId;
	
	private Long ccId;

	@JsonSerialize(using = DecimalJsonSerializer.class)
	private Double  amount;

	private PaymentStatus paymentStatus;

	private PaymentMode paymentMode;

	private String paymentRefNo;

	private String paymentDate;

	private String remarks;
	
	private Long cb;
	
	private LocalDateTime co;
	
	private Long mb;
	
	private LocalDateTime mo;
	
	private String paymentDateStr;
	
	private String paymentTimeStr;
	
	private String ccName;
	
	private String supplierName;
	
	private String supplierRegNo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public PaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(PaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getPaymentRefNo() {
		return paymentRefNo;
	}

	public void setPaymentRefNo(String paymentRefNo) {
		this.paymentRefNo = paymentRefNo;
	}

	public String getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public LocalDateTime getMo() {
		return mo;
	}

	public void setMo(LocalDateTime mo) {
		this.mo = mo;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	

	@JsonIgnore
	public Long getCb() {
		return cb;
	}

	public void setCb(Long cb) {
		this.cb = cb;
	}

	public LocalDateTime getCo() {
		return co;
	}

	public void setCo(LocalDateTime co) {
		this.co = co;
	}

	@JsonIgnore
	public Long getMb() {
		return mb;
	}

	public void setMb(Long mb) {
		this.mb = mb;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getCcId() {
		return ccId;
	}

	public void setCcId(Long ccId) {
		this.ccId = ccId;
	}

	public String getPaymentDateStr() {
		return paymentDateStr;
	}

	public void setPaymentDateStr(String paymentDateStr) {
		this.paymentDateStr = paymentDateStr;
	}

	public String getPaymentTimeStr() {
		return paymentTimeStr;
	}

	public void setPaymentTimeStr(String paymentTimeStr) {
		this.paymentTimeStr = paymentTimeStr;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierRegNo() {
		return supplierRegNo;
	}

	public void setSupplierRegNo(String supplierRegNo) {
		this.supplierRegNo = supplierRegNo;
	}

		

}
