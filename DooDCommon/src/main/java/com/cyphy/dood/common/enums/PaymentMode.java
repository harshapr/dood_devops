package com.cyphy.dood.common.enums;

public enum PaymentMode {

	DONT_USE_THIS(0),CASH(1),CHEQUE(2),UPI(3),NEFTORRTGS(4);
	
	
	int id = -1;
	
	private PaymentMode(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	public static PaymentMode getPaymentMode(long id) {
        for (PaymentMode es : PaymentMode.values()) {
            if (es.id == id) {
                return es;
            }
        }
        throw new IllegalArgumentException();
	}
}
