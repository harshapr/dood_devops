package com.cyphy.dood.common.vo;

import java.io.Serializable;
import java.sql.Timestamp;



public class RequestVO implements Serializable{

	private Object requestParams;
	
	private Timestamp requestReceivedAt = new Timestamp(System.currentTimeMillis());
	
	private Long loggedInUserId;
	
	public Object getRequestParams() {
		return requestParams;
	}

	public void setRequestParams(Object requestParams) {
		this.requestParams = requestParams;
	}

	public Timestamp getRequestReceivedAt() {
		return requestReceivedAt;
	}

	public void setRequestReceivedAt(Timestamp requestReceivedAt) {
		this.requestReceivedAt = requestReceivedAt;
	}

	public Long getLoggedInUserId() {
		return loggedInUserId;
	}

	public void setLoggedInUserId(Long loggedInUserId) {
		this.loggedInUserId = loggedInUserId;
	}

}
