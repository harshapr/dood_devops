package com.cyphy.dood.config.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class DoodConfigService {

private static final Logger LOGGER = LoggerFactory.getLogger(DoodConfigService.class);
	
	public static void main(String[] args) {
		try {
			LOGGER.info("\n\tStarting Configuration Service.....\n");
			SpringApplication.run(DoodConfigService.class, args);
			LOGGER.info("\n\n\t##################################Configuration Service Started Successfully######################################\n\n");
			System.out.println("Configuration Service Started Successfully");
		}catch(Exception e) {
			LOGGER.error("\n\t!!!! Error while starting Configuration Service.Check logs.\n",e);
		}
		
	}

}
