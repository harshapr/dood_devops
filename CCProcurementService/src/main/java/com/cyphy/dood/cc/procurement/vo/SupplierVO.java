package com.cyphy.dood.cc.procurement.vo;

import java.io.Serializable;
import java.sql.Timestamp;

import com.cyphy.dood.common.enums.Gender;
import com.cyphy.dood.common.enums.IDType;
import com.cyphy.dood.common.enums.MilkType;
import com.cyphy.dood.common.enums.Status;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
 
@JsonInclude(Include.NON_NULL)
public class SupplierVO implements Serializable {

	private Long id;
	
	private String regNumber;
	
	private Long ccId;
	
	private String name;

	private String address;

	private Long place;
	
	private Gender gender;

	private IDType idType;

	private String id_number;
	
	private Status status;
	
	private String phoneNumber;
	
	private String bankName;
	
	private String accountNumber;
	
	private String ifscCode;
	
	private String photo;
	
	private Long cowCount=0l;
	
	private Long buffaloCount=0l;
	
	private MilkType milkTypeSupply;

	private Long cb;
	
	private Timestamp co;
	
	private Long mb;
	
	private Timestamp mo;
	
	private double amtPaid;
	
	private double totalProcurementAmt;
	
	private String registeredOn;
	
	private String supplierPlaceName;
		
	private String ccName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getPlace() {
		return place;
	}

	public void setPlace(Long place) {
		this.place = place;
	}

	public IDType getIdType() {
		return idType;
	}

	public void setIdType(IDType idType) {
		this.idType = idType;
	}

	public String getId_number() {
		return id_number;
	}

	public void setId_number(String id_number) {
		this.id_number = id_number;
	}

	public Long getCb() {
		return cb;
	}

	public void setCb(Long cb) {
		this.cb = cb;
	}

	public Timestamp getCo() {
		return co;
	}

	public void setCo(Timestamp co) {
		this.co = co;
	}

	public Long getMb() {
		return mb;
	}

	public void setMb(Long mb) {
		this.mb = mb;
	}

	public Timestamp getMo() {
		return mo;
	}

	public void setMo(Timestamp mo) {
		this.mo = mo;
	}


	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Long getCcId() {
		return ccId;
	}

	public void setCcId(Long ccId) {
		this.ccId = ccId;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public double getAmtPaid() {
		return amtPaid;
	}

	public void setAmtPaid(double amtPaid) {
		this.amtPaid = amtPaid;
	}

	public double getTotalProcurementAmt() {
		return totalProcurementAmt;
	}

	public void setTotalProcurementAmt(double totalProcurementAmt) {
		this.totalProcurementAmt = totalProcurementAmt;
	}
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.regNumber;
	}

	public String getRegisteredOn() {
		return registeredOn;
	}

	public void setRegisteredOn(String registeredOn) {
		this.registeredOn = registeredOn;
	}

	public String getSupplierPlaceName() {
		return supplierPlaceName;
	}

	public void setSupplierPlaceName(String supplierPlaceName) {
		this.supplierPlaceName = supplierPlaceName;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Long getCowCount() {
		return cowCount;
	}

	public void setCowCount(Long cowCount) {
		this.cowCount = cowCount;
	}

	public Long getBuffaloCount() {
		return buffaloCount;
	}

	public void setBuffaloCount(Long buffaloCount) {
		this.buffaloCount = buffaloCount;
	}

	public MilkType getMilkTypeSupply() {
		return milkTypeSupply;
	}

	public void setMilkTypeSupply(MilkType milkTypeSupply) {
		this.milkTypeSupply = milkTypeSupply;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
}
