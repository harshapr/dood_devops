package com.cyphy.dood.common.endpoint;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Timestamp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import com.cyphy.dood.common.util.DoodException;
import com.cyphy.dood.common.vo.RequestVO;
import com.cyphy.dood.common.vo.ResponseVO;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AbstractEndPoint {
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	Environment environment;

	@Bean
	@Scope("prototype")
	public Logger logger(final InjectionPoint ip) {
		final Class loggerClass;
		if (null != ip.getMethodParameter()) {
			loggerClass = ip.getMethodParameter().getContainingClass();
		} else {
			loggerClass = ip.getField().getDeclaringClass();
		}
		return LogManager.getLogger(loggerClass);
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		return objectMapper;
	}
	
	@Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

	
	/**
	 * Set success response to true and Object if any to
	 * be returned
	 * @param returnObject
	 * @return
	 */
	protected ResponseVO setSuccessResponse(Object returnObject,RequestVO irakshanRequestVO) {
		ResponseVO responseVO = new ResponseVO();
		responseVO.setSuccess(true);
		responseVO.setReturnObject(returnObject);
		responseVO.setRequestTime(irakshanRequestVO.getRequestReceivedAt());
		responseVO.setResponseTime(new Timestamp(System.currentTimeMillis()));
		return responseVO;
	}
	
	
	/**
	 * Set success response to true and Object if any to
	 * be returned
	 * @param returnObject
	 * @return
	 */
	protected ResponseVO setErrorResponse(Throwable e,RequestVO requestVO) {
		ResponseVO responseVO = new ResponseVO();
		responseVO.setSuccess(false);
		if(e instanceof Exception) {
			responseVO.setErrorMessage(((DoodException)e).getAppMessage());
			if(((DoodException) e).getThrowable() != null) {//This comes null when business exception is thrown
				StringWriter sw = new StringWriter();
		        ((DoodException) e).getThrowable().printStackTrace(new PrintWriter(sw));
		        responseVO.setErrorStackTrace(sw.toString());
			}
		}
		responseVO.setRequestTime(requestVO.getRequestReceivedAt());
		responseVO.setResponseTime(new Timestamp(System.currentTimeMillis()));
		return responseVO;
	}
	
	
	/**
	 * 
	 * @param <T>
	 * @param requestVO
	 * @return
	 * @throws IrakshanException 
	 */
	protected <T> T getObjectFromRequestParam(RequestVO requestVO,Class<T> classToBeConverted) throws DoodException {
		try {
			if(requestVO != null)
				return objectMapper.convertValue(requestVO.getRequestParams(), classToBeConverted);
		} catch (Exception e) {
			throw new DoodException("Exception occorued while parsing input from client", e);
		}
		return null;
	}
}
