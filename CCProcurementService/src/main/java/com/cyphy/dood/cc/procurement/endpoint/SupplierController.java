package com.cyphy.dood.cc.procurement.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cyphy.dood.cc.procurement.service.SupplierService;
import com.cyphy.dood.cc.procurement.vo.SupplierVO;
import com.cyphy.dood.common.endpoint.AbstractEndPoint;
import com.cyphy.dood.common.util.DoodException;
import com.cyphy.dood.common.vo.RequestVO;



@RefreshScope
@RestController
@RequestMapping
public class SupplierController extends AbstractEndPoint{
    
    @Autowired
    SupplierService supplierService;
	
	
	/**
	 * Add/Update Supplier.
	 * @param RequestVO
	 * @return
	 * @throws IrakshanException
	 */
	@RequestMapping(value = { "addorupdatesupplier" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> addOrUpdateSupplierDetails(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			SupplierVO supplierVO = getObjectFromRequestParam(request, SupplierVO.class);
			supplierVO = supplierService.addOrUpdateSupplier(supplierVO);
			obj = setSuccessResponse(supplierVO, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	} 
	
	
	/**
	 * List Suppliers By CC
	 * @param RequestVO
	 * @return
	 * @throws IrakshanException
	 */
	@RequestMapping(value = { "listsuppliersbycc" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> listSuppliersByCC(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Long agentId = getObjectFromRequestParam(request, Long.class);
			List<SupplierVO> supplierList = supplierService.listCCSuppliers(agentId);
			obj = setSuccessResponse(supplierList, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	} 
	
	
	/**
	 * Delete Supplier
	 * @param RequestVO - Map<String,Object) - procurementId and remarks
	 * @return
	 * @throws DoodException
	 */
	@RequestMapping(value = { "deletesupplier" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> deleteSupplier(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Long  supplierId = getObjectFromRequestParam(request, Long.class);
			obj = supplierService.deleteSupplier(supplierId);
			obj = setSuccessResponse(obj, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	
	/**
	 * Get the supplier details by Supplier Reg No
	 * @param request
	 * @return
	 */
	@RequestMapping(value = { "getsupplierbyregno" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getSupplierByCode(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			String  supplierRegNo = getObjectFromRequestParam(request, String.class);
			obj = supplierService.getSupplierByRegNo(supplierRegNo);
			obj = setSuccessResponse(obj, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	/**
	 * Get the supplier details by Supplier Reg No
	 * @param request
	 * @return
	 */
	@RequestMapping(value = { "getsupplierbyid" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getSupplierById(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Long  supplierId = getObjectFromRequestParam(request, Long.class);
			obj = supplierService.getSupplierById(supplierId);
			obj = setSuccessResponse(obj, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	
	/**
	 * Get the supplier details by Supplier Mobile Number
	 * @param request
	 * @return
	 */
	@RequestMapping(value = { "getsupplierbymobilenumber" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getSupplierByMobileNumber(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			String  mobileNumber = getObjectFromRequestParam(request, String.class);
			obj = supplierService.getSupplierByMobileNumber(mobileNumber);
			obj = setSuccessResponse(obj, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	
	/**
	 * Get the supplier details by Name or Supplier Mobile Number
	 * @param request
	 * @return
	 */
	@RequestMapping(value = { "getsupplierbynameormobilenumber" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getSupplierByNameOrMobileNumber(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			String  nameOrmobileNumber = getObjectFromRequestParam(request, String.class);
			obj = supplierService.getSupplierByNameOrMobileNumber(nameOrmobileNumber);
			obj = setSuccessResponse(obj, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	}
	
	
}
