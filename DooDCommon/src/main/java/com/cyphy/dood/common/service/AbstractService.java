package com.cyphy.dood.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.cyphy.dood.common.vo.RequestVO;
import com.cyphy.dood.common.vo.ResponseVO;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;



public abstract class AbstractService {

	@Autowired
    private RestTemplate restTemplate;

    @Autowired
    private EurekaClient eurekaClient;
    
    
    private String REPOSITORY_SERVICE_ID = "RepositoryService";
    
    
    /**
     * Do post call to repositoryService
     * @param url
     * @param request
     * @return
     */
    public ResponseEntity<ResponseVO> doPostCall(String url,RequestVO request){
    //	Application application = eurekaClient.getApplication(REPOSITORY_SERVICE_ID);
       // InstanceInfo instanceInfo = application.getInstances().get(0);
        url = "http://" + System.getenv("REPOSITORY_SERVICE_HOST") + ":" + System.getenv("REPOSITORY_SERVICE_PORT")+ ""+url;
        System.out.println("URL" + url);
        ResponseEntity<ResponseVO> result = restTemplate.postForEntity(url, request, ResponseVO.class);
        return result;
    }
    
    
    /**
     * Do post call to other Service
     * @param url
     * @param request
     * @return
     */
    public ResponseEntity<ResponseVO> doPostCall(String service, String url,RequestVO request){
    	Application application = eurekaClient.getApplication(service);
        InstanceInfo instanceInfo = application.getInstances().get(0);
        url = "http://" + instanceInfo.getIPAddr() + ":" + instanceInfo.getPort() + ""+url;
        System.out.println("URL" + url);
        ResponseEntity<ResponseVO> result = restTemplate.postForEntity(url, request, ResponseVO.class);
        return result;
    }
    
    
    
     
}
