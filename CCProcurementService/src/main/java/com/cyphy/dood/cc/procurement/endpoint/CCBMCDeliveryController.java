package com.cyphy.dood.cc.procurement.endpoint;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cyphy.dood.cc.procurement.service.CCBMCDeliveryService;
import com.cyphy.dood.cc.procurement.vo.CCBMCDeliveryVO;
import com.cyphy.dood.common.endpoint.AbstractEndPoint;
import com.cyphy.dood.common.enums.DeliveryStatus;
import com.cyphy.dood.common.util.DoodException;
import com.cyphy.dood.common.vo.RequestVO;



@RefreshScope
@RestController
@RequestMapping
public class CCBMCDeliveryController extends AbstractEndPoint{
    
    @Autowired
    CCBMCDeliveryService ccBMCDeliveryService;
	
	/**
	 * Genrate CC to BMC Delivery challan
	 * @param RequestVO
	 * @return
	 * @throws IrakshanException
	 */
	@RequestMapping(value = { "generatedeliverychallan" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> addOrUpdateProcurementDetails(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			CCBMCDeliveryVO ccBMCDeliveryVO = getObjectFromRequestParam(request, CCBMCDeliveryVO.class);
			String challanRefNo = ccBMCDeliveryService.generateCCToBMCDeliveryChallan(ccBMCDeliveryVO);
			obj = setSuccessResponse(challanRefNo, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	} 
	
	
	/**
	 * Created Delivery challan to transport from CC to BMC
	 * @param RequestVO
	 * @return
	 */
	@RequestMapping(value = { "getdeliverydetailsbychallanrefno" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> getDeliveryDetailsByChallanRefNo(@RequestBody RequestVO request) {
		Object object = null;
		try {
			String deliveryChallanRefNo = getObjectFromRequestParam(request, String.class);
			object = ccBMCDeliveryService.getDeliveryDetailsByChallanRefNo(deliveryChallanRefNo);
			object = setSuccessResponse(object,request);
		} catch (Exception e) {
			e.printStackTrace();
			object = setErrorResponse(e,request);
		}
		return new ResponseEntity<T>((T) object, HttpStatus.OK);
	}
	
	/**
	 * List Deliveries by Village collection for current date
	 * @param request
	 * @return
	 */
	@RequestMapping(value = { "listdeliveriesbycc" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> listDeliveriesByCC(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Long ccId = getObjectFromRequestParam(request, Long.class);
			obj = ccBMCDeliveryService.listDeliveriesByCC(ccId);
			obj = setSuccessResponse(obj, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	} 
	
	
	/**
	 * List Deliveries by Village collection between dates
	 * @param request
	 * @return
	 */
	@RequestMapping(value = { "listdeliveriesbyccbetweendates" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> listDeliveriesByCCBetweenDates(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Map<String,Object> requestData = getObjectFromRequestParam(request, Map.class);
			obj = ccBMCDeliveryService.listDeliveriesByCCBetweenDates(((Integer)requestData.get("ccId")).longValue(),(requestData.get("fromDate").toString()),(requestData.get("toDate").toString()));;
			obj = setSuccessResponse(obj, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	} 
	
	
	/**
	 * Update the delivery status as received
	 * @param RequestVO
	 * @return
	 */
	@RequestMapping(value = { "updatedeliverystatusasreceived" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> updateDeliveryStatusAsReceived(@RequestBody RequestVO request) {
		Object object = null;
		try {
			Map<String, Object> requestData = getObjectFromRequestParam(request, Map.class);
			ccBMCDeliveryService.updateDeliveryStatus(((Integer)requestData.get("deliveryId")).longValue(),DeliveryStatus.RECEIVED_AND_ACCEPTED,(requestData.get("remarks").toString()),0);
			object = setSuccessResponse(object,request);
		} catch (Exception e) {
			e.printStackTrace();
			object = setErrorResponse(e,request);
		}
		return new ResponseEntity<T>((T) object, HttpStatus.OK);
	}
	
	/**
	 * Update the delivery status as cancelled
	 * @param RequestVO
	 * @return
	 */
	@RequestMapping(value = { "updatedeliverystatusascancelled" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> updateDeliveryStatusAsCancelled(@RequestBody RequestVO request) {
		Object object = null;
		try {
			Map<String, Object> requestData = getObjectFromRequestParam(request, Map.class);
			ccBMCDeliveryService.updateDeliveryStatus(((Integer)requestData.get("deliveryId")).longValue(),DeliveryStatus.CANCELLED,(requestData.get("remarks").toString()),0);
			object = setSuccessResponse(object,request);
		} catch (Exception e) {
			e.printStackTrace();
			object = setErrorResponse(e,request);
		}
		return new ResponseEntity<T>((T) object, HttpStatus.OK);
	}
	
	/**
	 * List Deliveries by BMC for current date
	 * @param request
	 * @return
	 */
	@RequestMapping(value = { "listdeliveriesbybmc" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> listDeliveriesByBMC(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Long bmcId = getObjectFromRequestParam(request, Long.class);
			obj = ccBMCDeliveryService.listDeliveriesByBMC(bmcId);
			obj = setSuccessResponse(obj, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	} 
	
	/**
	 * List Deliveries by bmc between dates
	 * @param request
	 * @return
	 */
	@RequestMapping(value = { "listdeliveriesbybmcbetweendates" }, method = RequestMethod.POST)
	public <T> ResponseEntity<T> listDeliveriesByBMCBetweenDates(@RequestBody RequestVO request) {
		Object obj = null;
		try {
			Map<String,Object> requestData = getObjectFromRequestParam(request, Map.class);
			obj = ccBMCDeliveryService.listDeliveriesByCCBetweenDates(requestData);
			obj = setSuccessResponse(obj, request);
		}catch(DoodException ie) {
			obj = setErrorResponse(ie, request);
		}catch(Exception ie) {
			ie.printStackTrace();
			obj = setErrorResponse(ie, request);
		}
		return new ResponseEntity<T>((T) obj, HttpStatus.OK);
	} 
	
}
