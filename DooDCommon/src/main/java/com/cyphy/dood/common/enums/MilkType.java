package com.cyphy.dood.common.enums;

public enum MilkType {

	DONT_USE_THIS(0),COW(1),BUFFALO(2),BOTH(3);
	
	
	int typeId = -1;
	
	private MilkType(int typeId) {
		this.typeId = typeId;
	}
	
	public int getTypeId() {
		return this.typeId;
	}
	
	public static MilkType getMilkType(long id) {
        for (MilkType es : MilkType.values()) {
            if (es.typeId == id) {
                return es;
            }
        }
        throw new IllegalArgumentException();
	}
}
