package com.cyphy.dood.common.otp.vo;

public class OTPSettings {

	private Long otpLength = new Long(6);

	private Long otpValidity = new Long(15); // in minutes

	public Long getOtpLength() {
		return otpLength;
	}

	public void setOtpLength(Long otpLength) {
		this.otpLength = otpLength;
	}

	public Long getOtpValidity() {
		return otpValidity;
	}

	public void setOtpValidity(Long otpValidity) {
		this.otpValidity = otpValidity;
	}

}
