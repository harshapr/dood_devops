package com.cyphy.dood.common.notification.vo;

import java.util.List;

public class NotificationVO {

	private NotificationType notificationType;

	private String messageHeader;

	private String message;

	private List<String> recipientList;

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public String getMessageHeader() {
		return messageHeader;
	}

	public void setMessageHeader(String messageHeader) {
		this.messageHeader = messageHeader;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getRecipientList() {
		return recipientList;
	}

	public void setRecipientList(List<String> recipientList) {
		this.recipientList = recipientList;
	}

}
