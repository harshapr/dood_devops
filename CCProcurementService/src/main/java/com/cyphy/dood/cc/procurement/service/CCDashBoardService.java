package com.cyphy.dood.cc.procurement.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.cyphy.dood.common.service.AbstractService;
import com.cyphy.dood.common.util.DoodException;
import com.cyphy.dood.common.vo.CCDashboardVO;
import com.cyphy.dood.common.vo.RequestVO;
import com.cyphy.dood.common.vo.ResponseVO;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class CCDashBoardService extends AbstractService {

	@Autowired
	ObjectMapper objectMapper;

	public CCDashboardVO fetchCCProcurementDashboardDetails(Map<String, Object> inputData) throws DoodException {

		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(inputData);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/cc/procurement/dashboard/procurementdetails", requestVO);
		CCDashboardVO ccDashboard = objectMapper.convertValue(result.getBody().getReturnObject(),
				CCDashboardVO.class);

		return ccDashboard;
	}

}
