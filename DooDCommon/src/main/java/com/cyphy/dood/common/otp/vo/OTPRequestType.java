package com.cyphy.dood.common.otp.vo;

public enum OTPRequestType {

	FORGOT_USERID, FORGOT_PASSWORD, MODIFY_PIN
}
