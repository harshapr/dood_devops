package com.cyphy.dood.common.enums;

public enum IDType {

	DONT_USE_THIS(0),AADHAR(1),DRIVING_LICENSE(2),VOTER_ID(3);
	
	
	int typeId = -1;
	
	private IDType(int typeId) {
		this.typeId = typeId;
	}
	
	public int getTypeId() {
		return this.typeId;
	}
	
	public static IDType getIDType(long id) {
        for (IDType es : IDType.values()) {
            if (es.typeId == id) {
                return es;
            }
        }
        throw new IllegalArgumentException();
	}
}
