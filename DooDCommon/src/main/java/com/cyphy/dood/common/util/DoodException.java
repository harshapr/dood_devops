package com.cyphy.dood.common.util;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class DoodException extends Exception{

	private String appMessage;

	private Throwable throwable;

	private Long errorCode;

	@Autowired
	 private Logger logger;
	
	public DoodException() {

	}

	public DoodException(String message) {
		super();
		this.appMessage = message;
	}
	
	public DoodException(String message, Throwable throwable) {
		super();
		this.appMessage = message;
		this.throwable = throwable;
		throwable.printStackTrace();
	}

	public DoodException(String message, Throwable throwable, Long errorCode) {
		super();
		this.appMessage = message;
		this.throwable = throwable;
		this.errorCode = errorCode;
		throwable.printStackTrace();
	}

	

	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

	public Long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Long errorCode) {
		this.errorCode = errorCode;
	}

	public String getAppMessage() {
		return appMessage;
	}

	public void setAppMessage(String appMessage) {
		this.appMessage = appMessage;
	}
}
