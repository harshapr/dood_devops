package com.cyphy.dood.microservice.discovery.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConnectionWaitComponent {

	@Value("${server.port}")
	private String serverPort;
}
