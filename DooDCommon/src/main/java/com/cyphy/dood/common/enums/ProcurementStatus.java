package com.cyphy.dood.common.enums;

public enum ProcurementStatus {

	DONT_USE_THIS(0),PROCURED_FROM_SUPPLIER(1),SUPPLIED_TO_BMC(2),PROCUREMENT_APPROVED(3),PROCUREMENT_REJECTED(4),PROCUREMENT_CANCELLED(5),PROCURED_FROM_CC(6);
	
	
	int id = -1;
	
	private ProcurementStatus(int id) {
		this.id = id;
	}
	
	public int getId() {
		return this.id;
	}
	
	public static ProcurementStatus getProcurementStatus(long id) {
        for (ProcurementStatus es : ProcurementStatus.values()) {
            if (es.id == id) {
                return es;
            }
        }
        throw new IllegalArgumentException();
	}
}
