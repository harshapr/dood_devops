package com.cyphy.dood.cc.procurement.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.cyphy.dood.common.endpoint.AbstractEndPoint;

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages={"com.cyphy.dood.cc","com.cyphy.irakshan"})
public class CCProcurementServiceMain extends AbstractEndPoint{

	public static void main(String[] args) {
		try {
			SpringApplication.run(CCProcurementServiceMain.class, args);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("*").allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS").allowedHeaders("*");
			}
//			@Override
//		    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		        registry.addResourceHandler("/app/**","/assets/**").addResourceLocations("classpath:/ums_client/app/","classpath:/ums_client/assets/");
//		    }
		};
	}

}
