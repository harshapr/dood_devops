package com.cyphy.dood.common.enums;

public enum Gender {

	DONT_USE_THIS(0),MALE(1),FEMALE(2),OTHERS(3);
	
	
	int typeId = -1;
	
	private Gender(int typeId) {
		this.typeId = typeId;
	}
	
	public int getTypeId() {
		return this.typeId;
	}
	
	public static Gender getGeder(long id) {
        for (Gender es : Gender.values()) {
            if (es.typeId == id) {
                return es;
            }
        }
        throw new IllegalArgumentException();
	}
}
