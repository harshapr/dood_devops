package com.cyphy.dood.cc.procurement.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.cyphy.dood.cc.procurement.endpoint.SupplierPaymentController;
import com.cyphy.dood.cc.procurement.vo.SupplierPaymentVO;
import com.cyphy.dood.cc.procurement.vo.SupplierVO;
import com.cyphy.dood.common.enums.PaymentStatus;
import com.cyphy.dood.common.service.AbstractService;
import com.cyphy.dood.common.util.DoodException;
import com.cyphy.dood.common.vo.RequestVO;
import com.cyphy.dood.common.vo.ResponseVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SupplierPaymentService extends AbstractService{

	@Autowired
	Logger logger;
	
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	SupplierService supplierService;
	
	
	/**
	 * Make payment supplier.
	 * @param agentToSupplierPayment
	 * @return
	 * @throws DoodException
	 */
	public SupplierPaymentVO makeOrUpdatePaymentToSupplier(SupplierPaymentVO agentToSupplierPayment) throws DoodException{
		if(agentToSupplierPayment.getId() != null) {
			agentToSupplierPayment.setMo(LocalDateTime.now());
			agentToSupplierPayment.setPaymentStatus(PaymentStatus.PAYMENT_COMPLETED);
		}else {
			agentToSupplierPayment.setCo(LocalDateTime.now());
			agentToSupplierPayment.setPaymentStatus(PaymentStatus.PAYMENT_COMPLETED);
			//DateTimeFormatter.ofPattern("dd/MM/yyyy").format(localDateTime);
			agentToSupplierPayment.setPaymentDate(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss").format(LocalDateTime.now()));
			agentToSupplierPayment.setPaymentRefNo("PARFNO_"+System.currentTimeMillis());
		}
		RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(agentToSupplierPayment);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/supplier/payment/makeorupdatepaymenttosupplier", requestVO);
		if(result.getBody().isSuccess()) {
			agentToSupplierPayment = objectMapper.convertValue(result.getBody().getReturnObject(), SupplierPaymentVO.class);
			 requestVO = new RequestVO();
			 requestVO.setRequestParams(agentToSupplierPayment.getId());
			 result = doPostCall("/repository/supplier/payment/getpaymentsdetailsbyid", requestVO);
			 agentToSupplierPayment = objectMapper.convertValue(result.getBody().getReturnObject(), SupplierPaymentVO.class);
		}else {
			throw new DoodException(result.getBody().getErrorMessage());
		}
		return agentToSupplierPayment;
	}
	
	
	/**
	 * Gets the Collection Center Dashboard Data.
	 * Dashboard shows Supplier pending payment details in the descending order(Amount to be paid).
	 * Input is Collection Center Id.
	 * 
	 */
	public List<SupplierVO> findPendingPaymentsOfCC(Long ccId) throws DoodException{
		//List<Object> pendingPayments = new ArrayList<Object>();
		
		//Getting all total amount paid details of all suppliers
		RequestVO requestVO = new RequestVO();
		Map<String,Object> requestData = new HashMap<String, Object>();
		requestData.put("ccId", ccId);
		requestVO.setRequestParams(requestData);
		
		//Getting totam amount paid details supplier wise
		ResponseEntity<ResponseVO> result = doPostCall("/repository/supplier/payment/getsupplierwiseamountpaiddetails", requestVO);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		List<SupplierVO> supplierWiseTotalAmtPaidDetails = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<SupplierVO>>() {});
		Map<Long, SupplierVO> supplierWiseTotalAmtPaidDetailsMap = supplierWiseTotalAmtPaidDetails.stream()
			      .collect(Collectors.toMap(SupplierVO::getId, Function.identity()));
		
		//Getting total amount of all the procurements supplierwise
		result = doPostCall("/repository/cc/procurement/getsupplierwisetotalprocurementamtdetails", requestVO);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		List<SupplierVO> supplierWiseTotalProcurementAmt = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<SupplierVO>>() {});
		
		for (SupplierVO supplierVO : supplierWiseTotalProcurementAmt) {
			supplierWiseTotalAmtPaidDetailsMap.get(supplierVO.getId()).setTotalProcurementAmt(supplierVO.getTotalProcurementAmt());
		}
		
		return supplierWiseTotalAmtPaidDetails;
	}
	
	/**
	 * Gets the last 10 payments data made to supplier with given id
	 * @param supplierId
	 * @return
	 * @throws Exception
	 */
    public List<SupplierPaymentVO> getPaymentMadeToSupplier(Long supplierId) throws DoodException{
    	RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(supplierId);
		
		ResponseEntity<ResponseVO> result = doPostCall("/repository/supplier/payment/getpaymentsmadetosupplier", requestVO);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		List<SupplierPaymentVO> supplierPaymentHistory = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<SupplierPaymentVO>>() {});
		return supplierPaymentHistory;
    }
    
    
    /**
     * Get pending and last paid amount and date of supplier.
     * @param supplierId
     * @return
     * @throws DoodException
     */
    public Map<String,Object> getPendingAndLastPaidAmtTBySupplier(Long supplierId) throws DoodException{
    	Map<String,Object> returnJson = new HashMap<String,Object>();
    	List<SupplierPaymentVO> supplierPaymentHistory = getPaymentMadeToSupplier(supplierId);
    	if(supplierPaymentHistory != null && !supplierPaymentHistory.isEmpty()) {
    		SupplierPaymentVO supplierPaymentVO = supplierPaymentHistory.get(0);
    		returnJson.put("lastPaidAmt", String.format("%.2f", supplierPaymentVO.getAmount()));
    		returnJson.put("lastPaidDate", supplierPaymentVO.getPaymentDateStr()+" "+supplierPaymentVO.getPaymentTimeStr());
    	}
    	
    	RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(supplierId);
		
		ResponseEntity<ResponseVO> result = doPostCall("/repository/supplier/payment/gettotalpaymentamountofsupplier", requestVO);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		Double totalAmountPaid = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<Double>() {});
		
		result = doPostCall("/repository/cc/procurement/gettotalamountofallprocurementsbysupplier", requestVO);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		Double totalProcurementAmount = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<Double>() {});
		
		if(totalAmountPaid == null) {
			totalAmountPaid = 0.00;
		}
		if(totalProcurementAmount == null) {
			totalProcurementAmount = 0.00;
		}
		if(totalProcurementAmount != null && totalAmountPaid != null)
			returnJson.put("pendingAmount", String.format("%.2f", (totalProcurementAmount-totalAmountPaid)));
		
		return returnJson;
    }
    
    /**
	 * Gets the supplier payment data between given dates.
	 * @param Long - supplierId,
	 * @param Date - fromDate,
	 * @param Date - toDate,
	 * @return
	 * @throws Exception
	 */
    public List<SupplierPaymentVO> getPaymentMadeToSupplierBetweenDates(Long supplierId,String fromDate,String toDate) throws DoodException{
    	RequestVO requestVO = new RequestVO();
    	Map<String,Object> requestData = new HashMap<String,Object>();
    	requestData.put("supplierId",supplierId);
    	requestData.put("fromDate",fromDate);
    	requestData.put("toDate",toDate);
		requestVO.setRequestParams(requestData);
		
		ResponseEntity<ResponseVO> result = doPostCall("/repository/supplier/payment/getpaymentsmadetosupplierbetweendates", requestVO);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		List<SupplierPaymentVO> supplierPaymentHistory = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<SupplierPaymentVO>>() {});
		return supplierPaymentHistory;
    }
    
    
    /**
	 * Gets the last 10 payments data made by collection center with given id
	 * @param supplierId
	 * @return
	 * @throws Exception
	 */
    public List<SupplierPaymentVO> getPaymentsMadeByCollectionCenter(Long ccId) throws DoodException{
    	RequestVO requestVO = new RequestVO();
		requestVO.setRequestParams(ccId);
		ResponseEntity<ResponseVO> result = doPostCall("/repository/supplier/payment/getpaymentsmadebycc", requestVO);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		List<SupplierPaymentVO> supplierPaymentHistory = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<SupplierPaymentVO>>() {});
		return supplierPaymentHistory;
    }
    
    
    
    /**
	 * Gets payments made by collection center between dates.
	 * @param supplierId
	 * @return
	 * @throws Exception
	 */
    public List<SupplierPaymentVO> getPaymentsMadeByCollectionCenterBetweenDates(Long ccId,String fromDate,String toDate) throws DoodException{
    	RequestVO requestVO = new RequestVO();
    	Map<String,Object> requestData = new HashMap<String,Object>();
    	requestData.put("ccId",ccId);
    	requestData.put("fromDate",fromDate);
    	requestData.put("toDate",toDate);
		requestVO.setRequestParams(requestData);
		
		ResponseEntity<ResponseVO> result = doPostCall("/repository/supplier/payment/getpaymentsmadebyccbetweendates", requestVO);
		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		List<SupplierPaymentVO> supplierPaymentHistory = objectMapper.convertValue(result.getBody().getReturnObject(), new TypeReference<List<SupplierPaymentVO>>() {});
		return supplierPaymentHistory;
    }
	
	/**
	 * Find the payment by payment Id.
	 * @param agentId
	 * @param paymentId
	 * @return
	 * @throws DoodException
	 */
	public SupplierPaymentVO findPaymentByPaymentId(Long agentId,Long paymentId) throws DoodException{
		return null;
	}
	
	/**
	 * Find the payment by payment ref number.
	 * @param agentId
	 * @param paymentRefNumber
	 * @return
	 * @throws DoodException
	 */
	public SupplierPaymentVO findPaymentByPaymentRefNumber(Long agentId,String paymentRefNumber) throws DoodException{
		return null;
	}
	
	/**
	 * Cancel Payment.Once cancelled cannot be reverted. 
	 * @param paymentId
	 * @return
	 * @throws DoodException
	 */
	public boolean cancelPayment(Long paymentId) throws DoodException{
		return false;
	}
	
}
