package com.cyphy.dood.microservice.discovery.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.ComponentScan;

@EnableEurekaServer
@SpringBootApplication
@ComponentScan("com.cyphy.dood.microservice.discovery")
public class DooDDiscoveryService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DooDDiscoveryService.class);
	
	public static void main(String[] args) {
		try {
		LOGGER.info("\n\tStarting Discovery Service.....\n");
		SpringApplication.run(DooDDiscoveryService.class, args);
		LOGGER.info("\n\n\t##################################Discovery Service Started Successfully######################################\n\n");
		System.out.println("Discovery Service Started Successfully");
		}
		catch(Exception e) {
			LOGGER.error("\n\t!!!! Error while starting Discovery Service..Check logs.\n",e);
			System.out.println("\\n\\t!!!! Error while starting Discovery Service. Restarting!!!!!.\\n");
		}
	}

}
