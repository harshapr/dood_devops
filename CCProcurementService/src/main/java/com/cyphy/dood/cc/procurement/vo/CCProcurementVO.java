package com.cyphy.dood.cc.procurement.vo;

import java.io.Serializable;
import java.sql.Timestamp;

import com.cyphy.dood.common.enums.MilkType;
import com.cyphy.dood.common.enums.ProcurementStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
 
@JsonInclude(Include.NON_NULL)
public class CCProcurementVO implements Serializable {
	
	private Long id;
	
	private String refNo;
	
	
	private Long ccId;

	private Long ccIncharge;


	private Long supplierId;

	private MilkType milkType;

	private Double quantity;

	private Double fat;

	private Double snf;

	private Double density;
	
	private Double amount;//Should be calculated automatically.
	
	//@JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Timestamp procurementDateTime;

	private ProcurementStatus procurementStatus;
	
	private Timestamp mo;
	
	private String remarks;
	
	private String ccName;

	private String supplierName;

	private String procuredBy;
	
	private String procuredOnDate;
	
	private String procuredOnTime;
	
	private String supplierRegNo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public MilkType getMilkType() {
		return milkType;
	}

	public void setMilkType(MilkType milkType) {
		this.milkType = milkType;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getFat() {
		return fat;
	}

	public void setFat(Double fat) {
		this.fat = fat;
	}

	public Double getSnf() {
		return snf;
	}

	public void setSnf(Double snf) {
		this.snf = snf;
	}

	public Double getDensity() {
		return density;
	}

	public void setDensity(Double density) {
		this.density = density;
	}

	public Timestamp getProcurementDateTime() {
		return procurementDateTime;
	}

	public void setProcurementDateTime(Timestamp procurementDateTime) {
		this.procurementDateTime = procurementDateTime;
	}

	public ProcurementStatus getProcurementStatus() {
		return procurementStatus;
	}

	public void setProcurementStatus(ProcurementStatus procurementStatus) {
		this.procurementStatus = procurementStatus;
	}

	public Timestamp getMo() {
		return mo;
	}

	public void setMo(Timestamp mo) {
		this.mo = mo;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getCcId() {
		return ccId;
	}

	public void setCcId(Long ccId) {
		this.ccId = ccId;
	}

	public Long getCcIncharge() {
		return ccIncharge;
	}

	public void setCcIncharge(Long ccIncharge) {
		this.ccIncharge = ccIncharge;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getProcuredBy() {
		return procuredBy;
	}

	public void setProcuredBy(String procuredBy) {
		this.procuredBy = procuredBy;
	}

	public String getProcuredOnDate() {
		return procuredOnDate;
	}

	public void setProcuredOnDate(String procuredOnDate) {
		this.procuredOnDate = procuredOnDate;
	}

	public String getProcuredOnTime() {
		return procuredOnTime;
	}

	public void setProcuredOnTime(String procuredOnTime) {
		this.procuredOnTime = procuredOnTime;
	}

	public String getSupplierRegNo() {
		return supplierRegNo;
	}

	public void setSupplierRegNo(String supplierRegNo) {
		this.supplierRegNo = supplierRegNo;
	}
	

}
